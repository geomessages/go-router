Router.go
=====================

This is a small library to help with the dreaded URL parsing in a web projects. 

The project is mainly a learning project for me to write some non-trivial go. 
It should however be useful since I think it provides some interesting ideas not found in 
the other existing frameworks.

Components
----------------------

The parameter count has to match the one in
the provided mapping URL. If it doesn't the registration will panic.

"/app/:id/attr/:key" = func(c *Context, param1 int, param2 string)
"/app/:id/attr/:key/*" = func(c *Context, param1 int, param2 string)
"/app/:id/attr/:key/*" = func(c *Context, param1 int, param2 string, path string)
