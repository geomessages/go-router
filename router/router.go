package router

import (
	"os"
	"fmt"
	"log"
	"strconv"
	"strings"
	"reflect"
    "net/http"
)

var Logger *log.Logger = log.New(os.Stdout,"router ",log.LstdFlags)

func callHandler(c interface{}, params []string, handlerFunc interface{}) (bool, *Error) {
	handlerType := reflect.TypeOf(handlerFunc)
	numIn := handlerType.NumIn()
	if len(params)+1 != numIn {
		return false , new(Error)
	}
	paramValues := make([]reflect.Value,numIn)
	paramValues[0] = reflect.ValueOf(c)
	for i := 1; i < numIn; i++ {
		argType := handlerType.In(i)
		param   := params[i-1]
		if argType == reflect.TypeOf(param) {
			paramValues[i] = reflect.ValueOf(param)
		} else {
			if argType == reflect.TypeOf(1) {
				convVal, convErr := strconv.Atoi(param)
				if convErr != nil {
					return false, new(Error)
				} 
				paramValues[i] = reflect.ValueOf(convVal)
			} else {
				return false, new(Error)
			}
		}
	}
	reflect.ValueOf(handlerFunc).Call(paramValues)
	return true, nil
}

type Error struct {
	message string
}

type HttpContext struct {
	ResponseWriter http.ResponseWriter
	Request *http.Request
}

type handler interface{}

func findHandler(handlers map[string] handler, path string) (handler, []string)  {
	for pattern, handler := range handlers { 
		params := extractParams(pattern,path)
		if params != nil {
			return handler, params
		}
	}
	return nil,nil
}

type router struct {
	contextConstructor func() interface{}
	getHandler map[string] handler
	postHandler map[string] handler
	putHandler map[string] handler
	deleteHandler map[string] handler
	headHandler map[string] handler	
	allHandlers map[string] handler
	filter []interface{}		
	rootPath string
}

func NewRouter(contextFactory func() interface{}) *router {
	r := new(router)
	r.contextConstructor = contextFactory
	r.allHandlers = make(map[string] handler)
	r.headHandler = make(map[string] handler)	
	r.getHandler = make(map[string] handler)
	r.postHandler = make(map[string] handler)
	r.putHandler = make(map[string] handler)
	r.deleteHandler = make(map[string] handler)
	r.headHandler = make(map[string] handler)
	r.rootPath = ""
	return r
}

func (r *router) SetRootPath(path string) {	
	for path != "" && path[len(path)-1] == '/' {
			path = path[0:len(path)-1]
	}
	r.rootPath = path
}

func (this *router) match(method string, path string) (handler, []string) {
	switch method {
		case "GET":
			return findHandler(this.getHandler,path)
		case "POST":
			return findHandler(this.postHandler,path)
		case "PUT":
			return findHandler(this.putHandler,path)
		case "DELETE":
			return findHandler(this.deleteHandler,path)
		case "HEAD":
			return findHandler(this.headHandler,path)			
	}
	return nil,nil
}

func (this *router) Get(pattern string, h handler) {
	this.getHandler[pattern] = h
	this.allHandlers[pattern] = h
}

func (this *router) Post(pattern string, h handler) {
	this.postHandler[pattern] = h
	this.allHandlers[pattern] = h
}

func (this *router) Put(pattern string, h handler) {
	this.putHandler[pattern] = h
	this.allHandlers[pattern] = h
}

func (this *router) Delete(pattern string, h handler) {
	this.deleteHandler[pattern] = h
	this.allHandlers[pattern] = h
}

func (this *router) Head(pattern string, h handler) {
	this.headHandler[pattern] = h
	this.allHandlers[pattern] = h
}

func (r *router) AddContextInit(filter interface{}) {
	r.filter = append(r.filter, filter)
}

func (this *router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if strings.Index(r.URL.Path,this.rootPath) == -1 {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "Path %v not found\n",r.URL.Path)
		return
	}
	path := r.URL.Path[len(this.rootPath):]
	handler, params := this.match(r.Method,path)
	if handler != nil {
		ctx := this.contextConstructor()

		var httpCtx HttpContext
		iface, _ := getStructPointer(reflect.TypeOf(httpCtx),reflect.ValueOf(ctx))
		htx := iface.(*HttpContext)
		htx.ResponseWriter = w
		htx.Request = r
		//fmt.Printf("%v\n",httpCtx)
		//fmt.Printf("%v\n",ctx)
		chain := NewChain()
		chain.GetFunc = func(i int, c *Chain) func() {
			
			if i > len(this.filter) {
				return nil
			}

			if (i == len(this.filter)) {
				return func() {
					callHandler(ctx,params,handler)
				}
			}
			
			filter := this.filter[i]
			paramCount := reflect.TypeOf(filter).NumIn()
			if paramCount < 2 {
				panic("Wrong param count")
				return nil
			} else if paramCount == 2 {
				return func() {
					filter.(func(*HttpContext,*Chain))(htx,c)
				}
			} 

			params := make([]reflect.Value,paramCount)
			params[0] = reflect.ValueOf(htx)
			params[paramCount-1] = reflect.ValueOf(c)

			filterType := reflect.TypeOf(filter)
			for i := 1; i < paramCount-1; i++ {
				inType := filterType.In(i).Elem()
				iface, _ := getStructPointer(inType,reflect.ValueOf(ctx))
				//fmt.Printf("%v\n",err)
				//fmt.Printf("%v\n",inType)
				params[i] = reflect.ValueOf(iface)
			}

			return func() {
				reflect.ValueOf(filter).Call(params)
			}
		}
		chain.Execute()
		if chain.errCode != -1 {
			fmt.Fprintf(w, chain.errMsg)
		}
		
	} else {
		handler, _ := findHandler(this.allHandlers,path)
		if handler != nil {
			w.WriteHeader(http.StatusMethodNotAllowed)
			fmt.Fprintf(w, "Method Not Allowed\n")
		} else {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "Resource %v Not found\n",path)
		}
	}
}
